package com.devcamp.db_relative_car_cartype.service;

import java.util.ArrayList;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.db_relative_car_cartype.model.CCar;
import com.devcamp.db_relative_car_cartype.model.CCarType;
import com.devcamp.db_relative_car_cartype.repository.ICarRepository;

@Service
public class CarService {
  @Autowired
  ICarRepository pCarRepository;

  public ArrayList<CCar> getAllCars() {
    ArrayList<CCar> listCar = new ArrayList<>();
    pCarRepository.findAll().forEach(listCar::add);
    return listCar;
  }

  public Set<CCarType> getCarTypeByCarCode(String carCode) {
    CCar vCar = pCarRepository.findByCarCode(carCode);
    if (vCar != null) {
      return vCar.getTypes();
    } else {
      return null;
    }
  }

}
