package com.devcamp.db_relative_car_cartype.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.db_relative_car_cartype.model.CCarType;
import com.devcamp.db_relative_car_cartype.repository.ICarTypeRepository;

@Service
public class CarTypeService {
  @Autowired
  ICarTypeRepository pCarTypeRepository;

  public ArrayList<CCarType> getAllCarType() {
    ArrayList<CCarType> pCCarTypes = new ArrayList<>();
    pCarTypeRepository.findAll().forEach(pCCarTypes::add);
    return pCCarTypes;
  }
}