package com.devcamp.db_relative_car_cartype.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.db_relative_car_cartype.model.CCar;

public interface ICarRepository extends JpaRepository<CCar, Long> {
  CCar findByCarCode(String carCode);
}
