package com.devcamp.db_relative_car_cartype.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.db_relative_car_cartype.model.CCarType;

public interface ICarTypeRepository extends JpaRepository<CCarType, Long> {

}
