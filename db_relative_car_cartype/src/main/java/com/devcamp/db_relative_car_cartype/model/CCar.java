package com.devcamp.db_relative_car_cartype.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "car")
public class CCar {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int car_id;

  @Column(name = "car_code", unique = true)
  private String carCode;

  @Column(name = "car_name")
  private String carName;

  @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
  @JsonManagedReference
  private Set<CCarType> types;

  public CCar() {
  }

  public CCar(int car_id, String carCode, String carName, Set<CCarType> types) {
    this.car_id = car_id;
    this.carCode = carCode;
    this.carName = carName;
    this.types = types;
  }

  public int getId() {
    return car_id;
  }

  public void setId(int car_id) {
    this.car_id = car_id;
  }

  public String getCarCode() {
    return carCode;
  }

  public void setCarCode(String carCode) {
    this.carCode = carCode;
  }

  public String getCarName() {
    return carName;
  }

  public void setCarName(String carName) {
    this.carName = carName;
  }

  public Set<CCarType> getTypes() {
    return types;
  }

  public void setTypes(Set<CCarType> types) {
    this.types = types;
  }

}
