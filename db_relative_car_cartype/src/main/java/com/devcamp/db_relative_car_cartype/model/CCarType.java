package com.devcamp.db_relative_car_cartype.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "carType")
public class CCarType {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int carType_id;

  @Column(name = "type_code")
  private String typeCode;

  @Column(name = "type_name")
  private String typeName;

  @ManyToOne
  @JsonIgnore
  @JoinColumn(name = "car_id")
  private CCar car;

  public CCarType() {
  }

  public CCarType(int carType_id, String typeCode, String typeName, CCar car) {
    this.carType_id = carType_id;
    this.typeCode = typeCode;
    this.typeName = typeName;
    this.car = car;
  }

  public int getId() {
    return carType_id;
  }

  public void setId(int carType_id) {
    this.carType_id = carType_id;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  public CCar getCar() {
    return car;
  }

  public void setCar(CCar car) {
    this.car = car;
  }

}
