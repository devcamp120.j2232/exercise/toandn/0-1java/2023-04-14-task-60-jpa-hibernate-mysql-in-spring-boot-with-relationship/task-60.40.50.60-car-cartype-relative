package com.devcamp.db_relative_car_cartype.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.db_relative_car_cartype.model.CCar;
import com.devcamp.db_relative_car_cartype.model.CCarType;
import com.devcamp.db_relative_car_cartype.service.CarService;
import com.devcamp.db_relative_car_cartype.service.CarTypeService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CarController {
  @Autowired
  CarService carService;

  @Autowired
  CarTypeService carTypeService;

  @GetMapping("/devcamp-cars")
  public ResponseEntity<List<CCar>> getAllCars() {
    try {
      return new ResponseEntity<>(carService.getAllCars(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/devcamp-cartypes")
  public ResponseEntity<Set<CCarType>> getCarTypeByCarCode(@RequestParam(value = "carCode") String carCode) {
    try {
      Set<CCarType> carTypes = carService.getCarTypeByCarCode(carCode);
      if (carTypes != null) {
        return new ResponseEntity<>(carTypes, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
