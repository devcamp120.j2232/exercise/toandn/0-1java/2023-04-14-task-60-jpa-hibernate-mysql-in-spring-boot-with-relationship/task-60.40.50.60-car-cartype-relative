package com.devcamp.db_relative_car_cartype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbRelativeCarCartypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbRelativeCarCartypeApplication.class, args);
	}

}
